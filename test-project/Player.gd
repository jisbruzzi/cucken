extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func in_floor():
	return get_node("KinematicBody2D").is_on_floor()

func _input(event:InputEvent):
	if event is InputEventScreenTouch or event is InputEventMouseButton:
		if in_floor():
			speed.y=-10000*4

var moving_right=true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

var speed=Vector2(0,0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if in_floor() and moving_right:
		speed.x=10000*2
	if in_floor() and not moving_right:
		speed.x=-10000*2
		
	if not in_floor():
		speed.y+=1000
		
	if get_node("KinematicBody2D/Right").is_colliding():
		moving_right=false
	
	if get_node("KinematicBody2D/Left").is_colliding():
		moving_right=true
		
	get_node("KinematicBody2D").move_and_slide(speed*delta,Vector2(0,-1))
		
		
